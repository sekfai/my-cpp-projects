#include <iostream>
#include "strlib.h"
#include "console.h"
#include "queue.h"
#include "vector.h"
#include "lexicon.h"
#include "simpio.h"

using namespace std;

Vector<string> getValidWords(const Lexicon& L, const string& word);
Vector<string> breadthFirstSearch(const string& sourceWord, const string& destinationWord, const Lexicon &L);

int main() {
    Lexicon L("testing2.txt");
    cout << "My Lexicon: " << L << endl;
//    Lexicon L("EnglishWords.dat");
    while (true) {
        string sourceWord = toLowerCase(getLine("Please enter the source word [return to quit]: "));
        if (sourceWord == "") {
            break;
        } else {
            string destinationWord = toLowerCase(getLine("Please enter the destination word [return to quit]: "));
            if (destinationWord == "") {
                break;
            } else {
                Vector<string> wordLadder = breadthFirstSearch(sourceWord, destinationWord, L);
                if (wordLadder.size() == 1) {
                    cout << "Ladder not found..." << endl;
                } else {
                    cout << "Found ladder: ";
                    for (string w: wordLadder) {
                        cout << w << ' ';
                    }
                    cout << endl;
                }
            }
        }
    }
    return 0;
}

Vector<string> getValidWords(const Lexicon& L, const string& word) {
    string alphabet = "abcdefghijklmnopqrstuvwxyz";
    Vector<string> v; // with invalid words (aaa, bbb, ccc)
    for (int i = 0; i < word.length(); i++) {
        for (int x = 0; x < alphabet.length(); x++) {
            string w = word;
            w[i] = alphabet[x];
            if (w != word) {
                v.add(w);
            }
        }
    }

    Vector<string> validWords;
    for (string w : v) {
        if (L.contains(w)) {
            validWords.add(w);
        }
    }
    return validWords;
}

// sourceWord = cat, destinationWord = cot
Vector<string> breadthFirstSearch(const string& sourceWord, const string& destinationWord, const Lexicon& L) {
    Lexicon usedWords;
    usedWords.add(sourceWord); // {cat}
    Queue<Vector<string> > q;
    Vector<string> initialLadder {sourceWord}; // {cat}
    q.enqueue(initialLadder);
    while (!q.isEmpty()) {
        Vector<string> ladder = q.dequeue(); // {cat} // {cat, eat} // {cat, cot}
        string topWord = ladder[ladder.size() - 1]; // cat // eat // cot

        if (topWord == destinationWord) {
            return ladder;
        } else {
            Vector<string> validWords = getValidWords(L, topWord); // {eat, cot} // {cat, ear}

            for (string w : validWords) {
                if (!usedWords.contains(w)) {
                    Vector<string> partialLadder = ladder;
                    partialLadder.add(w); // {cat, eat} // {cat, eat, ear}
                    q.enqueue(partialLadder); // {{cat, eat}, {cat, cot}} // {{cat, cot}, {cat, eat, ear}}
                    usedWords.add(w); // {cat, eat, cot} // {cat, eat, cot, ear}
                }
            }
        }
    }
    return initialLadder;
}
