#include <iostream>
#include "console.h"

using namespace std;

struct ListNode {
    ListNode* prev;
    int data;
    ListNode* next;
    ListNode() {
        data = 0;
        prev = nullptr;
        next = nullptr;
    }
    ListNode(int i) {
        data = i;
        prev = nullptr;
        next = nullptr;
    }
};

struct DoublyLinkedList {
    ListNode* front;
    ListNode* back;
    DoublyLinkedList() {
        front = nullptr;
        back = nullptr;
    }
};

void insertEnd(DoublyLinkedList& list, int value);
void printList(DoublyLinkedList list);

int main() {
    DoublyLinkedList a;
    cout << a.back << endl;
    cout << a.front << endl;
    insertEnd(a, 14);
    insertEnd(a, 176);
    insertEnd(a, 559);
    printList(a);
    return 0;
}

void insertEnd(DoublyLinkedList& list, int value) {
    ListNode* newNode = new ListNode(value);
    if (list.front == nullptr) {    // empty list
        list.front = newNode;
        list.back = newNode;
    } else {    // non empty list
        newNode->prev = list.back;
        list.back->next = newNode;
        list.back = newNode;
    }
}

void printList(DoublyLinkedList list) {
    ListNode* f = list.front;
    while (f != nullptr) {
        cout << f->data << " ";
        f = f->next;
    }
    cout << endl;

    ListNode* b = list.back;
    while (b != nullptr) {
        cout << b->data << " ";
        b = b->prev;
    }
    cout << endl;
}

//struct ListNode {
//    int data;
//    ListNode* next;
//};

//void toString(ListNode* front);
//int size(ListNode* front);
//int get(ListNode* front, int index);
//void add(ListNode*& front, int value);
//void addFirst(ListNode*& front, int value);
//void removeFirst(ListNode*& front);
//void remove(ListNode*& front, int index);

//int main() {
//    ListNode* front = nullptr;
//    add(front, 10);
//    add(front, 30);
//    add(front, 50);
//    add(front, 70);

//    toString(front);
//    cout << endl;
//    cout << "size : " << size(front) << endl;

//    for (int i = 0; i < size(front); i++) {
//        cout << "index " << i << " : " << get(front, i) << endl;
//    }

//    addFirst(front, 77);
//    toString(front);
//    cout << endl;

//    removeFirst(front);
//    toString(front);
//    cout << endl;

//    remove(front, 3);
//    toString(front);
//    return 0;
//}



//void toString(ListNode* front) {
//    while (front) {
//        cout << front->data << " ";
//        front = front->next;
//    }
//}

//int size(ListNode* front) {
//    int count = 0;
//    while (front != nullptr) {
//        front = front->next;
//        count++;
//    }
//    return count;
//}

//int get(ListNode* front, int index) {
//    if (front == nullptr) { // empty list
//        throw index;
//    } else {
//        // non-empty list
//        int count = 0;
//        while (front != nullptr) {
//            if (count == index) {
//                return front->data;
//            }
//            front = front->next;
//            count++;
//        }
//        throw index;
//    }
//}

//void add(ListNode*& front, int value) {
//    ListNode* newNode = new ListNode();
//    newNode->data = value;

//    if (front == nullptr) {
//        // empty list
//        front = newNode;
//    } else {
//        // non-empty list
//        ListNode* curr = front;
//        while (curr->next != nullptr) {
//            curr = curr->next;
//        }
//        curr->next = newNode;
//    }
//}

//void addFirst(ListNode*& front, int value) {
//    ListNode* newNode = new ListNode();
//    newNode->data = value;

//    if (front == nullptr) {
//        front = newNode;
//    } else {
//        newNode->next = front;
//        front = newNode;
//    }
////    one line solution
////    front = new ListNode(value, front);
//}

//// Removes the first value in the list
//// If list is empty, does nothing
//void removeFirst(ListNode*& front) {
//    if (front != nullptr) {
//        ListNode* trash = front;
//        front = front->next;
//        delete trash;
//    }
//}

//void remove(ListNode*& front, int index) {
//    if (index == 0) { // remove first element in the list
//        ListNode* trash = front;
//        front = front->next;
//        delete trash;
//    } else {
//        ListNode* curr = front;
//        for (int i = 0; i < index - 1; i++) {
//            curr = curr->next;
//        }
//        ListNode* trash = curr->next;
//        curr->next = curr->next->next;
//        delete trash;
//    }
//}
