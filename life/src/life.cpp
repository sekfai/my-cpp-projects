#include <iostream>
#include <string>
#include <fstream>
#include "console.h"
#include "random.h"
#include "simpio.h"
#include "grid.h"
#include "filelib.h"
#include "queue.h"
#include "gevents.h"
#include "life-constants.h"
#include "life-graphics.h"

using namespace std;

void generateRandomGrid(Grid<int>& board, const int& low = 40, const int& high = 60);
void generateGridFromFile(const string& filename, Grid<int>& board);
void updateDisplay(LifeDisplay& display, const Grid<int>& board);
void runSimulation(LifeDisplay& display, Grid<int>& board);
int countLiveNeighbors(const Grid<int>& board, const int& r, const int& c);
void advanceBoard(Grid<int>& board);
void autoSimulation(LifeDisplay& display, Grid<int>& board, const int &simulationSpeed);



int main() {
    Grid<int> board;
    if (getYesOrNo("Use random board? [Y/n]", "", "Y")) {
        generateRandomGrid(board);
    } else {
        string filename = getLine("Type a file name:");
        generateGridFromFile(filename, board);
    }
    bool autoMode = getYesOrNo("Auto mode? [Y/n]", "", "Y");
    cout << "Number of rows : " <<board.numRows() << endl;
    cout << "Number of columns : " <<board.numCols() << endl;
    LifeDisplay display;
    updateDisplay(display, board);
    if (autoMode) {
        int simulationSpeed = getIntegerBetween("Simulation Speed? [slow 1 - 3 fast)]", 1, 3);
        autoSimulation(display, board, simulationSpeed);
    } else {
        runSimulation(display, board);
    }
    return 0;
}

void generateRandomGrid(Grid<int>& board, const int &low, const int &high) {
    int nRows = randomInteger(low, high);
    int nCols = randomInteger(low, high);
    board.resize(nRows, nCols);
    for (int r = 0; r < board.numRows(); r++) {
        for (int c = 0; c < board.numCols(); c++) {
            board[r][c] = randomBool();
        }
    }
}

void generateGridFromFile(const string& filename, Grid<int>& board) {
    if (fileExists(filename)) {
        ifstream input;
        input.open(filename);
        int nRows;
        int nCols;
        input >> nRows >> nCols;
        board.resize(nRows, nCols);
        string line;
        Queue<char> q;
        while (getline(input, line)) {
            for (char character : line) {
                q.enqueue(character);
            }
        }

        for (int r = 0; r < board.numRows(); r++) {
            for (int c = 0; c < board.numCols(); c++) {
                char character = q.dequeue();
                if (character == '-') {
                    board[r][c] = 0;
                } else {
                    board[r][c] = 1;
                }
            }
        }
    } else {
        cout << "file not exist!" << endl;
    }
}

void updateDisplay(LifeDisplay& display, const Grid<int>& board) {
    display.setDimensions(board.numRows(), board.numCols());
    for (int r = 0; r < board.numRows(); r++) {
        for (int c = 0; c < board.numCols(); c++) {
            display.drawCellAt(r, c, board[r][c]);
        }
    }
}

void runSimulation(LifeDisplay& display, Grid<int>& board) {
    while (true) {
        GKeyEvent e = waitForEvent(KEY_EVENT);
        int keyCode = e.getKeyCode();
        char keyChar = e.getKeyChar();
        if (keyCode == 10) {
            advanceBoard(board);
            updateDisplay(display, board);
//            cout << "how are you?" << endl;
        } else if (keyChar == 'q') {
            display.~LifeDisplay();
            return;
        } else {
            // do nothing
        }
    }
}

int countLiveNeighbors(const Grid<int>& board, const int& r, const int& c) {
    int numLiveNeighbors = 0;
    if (board.inBounds(r - 1, c - 1)) { // top left
        if (board[r - 1][c - 1]) {
            numLiveNeighbors++;
        }
    }
    if (board.inBounds(r - 1, c)) { // top middle
        if (board[r - 1][c]) {
            numLiveNeighbors++;
        }
    }
    if (board.inBounds(r - 1, c + 1)) { // top right
        if (board[r - 1][c + 1]) {
            numLiveNeighbors++;
        }
    }
    if (board.inBounds(r, c - 1)) { // middle left
        if (board[r][c - 1]) {
            numLiveNeighbors++;
        }
    }
    if (board.inBounds(r, c + 1)) { // middle right
        if (board[r][c + 1]) {
            numLiveNeighbors++;
        }
    }
    if (board.inBounds(r + 1, c - 1)) { // bottom left
        if (board[r + 1][c - 1]) {
            numLiveNeighbors++;
        }
    }
    if (board.inBounds(r + 1, c)) { // bottom middle
        if (board[r + 1][c]) {
            numLiveNeighbors++;
        }
    }
    if (board.inBounds(r + 1, c + 1)) { // bottom right
        if (board[r + 1][c + 1]) {
            numLiveNeighbors++;
        }
    }
    return numLiveNeighbors;
}

void advanceBoard(Grid<int>& board) {
    Grid<int> tempBoard = board;
    for (int r = 0; r < board.numRows(); r++) {
        for (int c = 0; c < board.numCols(); c++) {
            bool isAlive = board[r][c];
            int numLiveNeighbors = countLiveNeighbors(board, r, c);
            if (isAlive && numLiveNeighbors <= 1) { // contains a cell but underpopulation
                tempBoard[r][c] = 0;
            } else if (isAlive && numLiveNeighbors == 2) { // contains a cell and stable
                tempBoard[r][c]++;
            } else if (!isAlive && numLiveNeighbors == 2) { // without a cell but stable
                // do nothing
            } else if (numLiveNeighbors == 3) { // perfect to stay
                tempBoard[r][c]++;
            } else { // overcrowding
                tempBoard[r][c] = 0;
            }
        }
    }
    board = tempBoard;
}

void autoSimulation(LifeDisplay& display, Grid<int>& board, const int& simulationSpeed) {
    while (true) {
        advanceBoard(board);
        updateDisplay(display, board);
        if (simulationSpeed == 1) {
            pause(1000);
        } else if (simulationSpeed == 2) {
            pause(500);
        } else {
            // no delay
        }
//        GKeyEvent e = waitForEvent(KEY_EVENT);
//        char keyChar = e.getKeyChar();
//        if (keyChar == 'q') {
//            display.~LifeDisplay();
//            return;
//        }
    }
}
